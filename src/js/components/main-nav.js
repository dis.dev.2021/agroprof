export default () => {
    const pageNavToggle = document.querySelectorAll('.nav-toggle');

    if (pageNavToggle) {
        pageNavToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = document.querySelector('.root');
                self.classList.toggle('nav-open');
                return false;
            });
        });
    }

    $('.mobile-nav__toggle').on('click', function (e) {
        e.preventDefault();
        let navItem = $(this).closest('.mobile-nav__item');
        let navGroup = $(this).closest('li');
        navItem.toggleClass('active');
        navGroup.find('>ul').slideToggle('fast');
    });

    $(".nav-dropdown")
        .mouseenter(function() {
            console.log('mouseenter');
            $('.nav-layout').addClass('open');
        })
        .mouseleave(function(){
            console.log('mouseleave');
            $('.nav-layout').removeClass('open');
        });
};
