import './plugins/import-jquery';
import 'focus-visible';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import mainNav from './components/main-nav';
import collapce from './components/collapse';
import maskPhone from './plugins/phone-mask';
import tabs from './components/tabs';
import SimpleBar from 'simplebar';
import accordion from './components/accordion';
import Choices from 'choices.js';
import '@fancyapps/fancybox'
import {WebpMachine} from "webp-hero"
import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);

documentReady(() => {
    lazyImages();
    tabs();
    mainNav();
    collapce();
    accordion();
    maskPhone('input[name="phone"]');

    // IE Webp Support
    const webpMachine = new WebpMachine()
    webpMachine.polyfillDocument()

    // Select
    const selectElem = document.querySelector('.pretty-select');
    if (selectElem) {
        const prettySelect = new Choices(selectElem);
    }

    // Modal
    $('.btn-modal').fancybox({
        autoFocus: false,
    });

    // Custom scroll
    document.querySelectorAll('.custom-scroll').forEach(el => {
        new SimpleBar(el)
    });

    $('.search-bar__toggle').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.search-bar').toggleClass("open");
    });

    $('body').on('click', function (event) {

        if ($(event.target).closest(".search-bar").length === 0) {
            $(".search-bar").removeClass('open');
        }
    });

    // Footer nav
    $('.footer-nav__header--icon').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('open')
        $(this).closest('.footer-nav__group').find('.footer-nav__content').slideToggle('fast');
    });

    function stickyBar() {
        const bar = document.querySelector('.bar');
        if(bar) {
            const stickyLabel = document.querySelector('.bar');
            let  $h = $(stickyLabel).offset().top;

            $(window).scroll(function () {
                if ($(window).scrollTop() > $h) {
                    $(bar).addClass('fix-top');
                } else {
                    $(bar).removeClass('fix-top');
                }
            });
        }
    }
    stickyBar();

    const timeLine = new Swiper('.time-line__slider', {
        slidesPerView: 'auto',
        spaceBetween: 0,
    })

    const features = new Swiper('.block-features__slider', {
        slidesPerView: 'auto',
        spaceBetween: 20,
    })

    const homeSlider = new Swiper('.home-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.home-slider__nav--next',
            prevEl: '.home-slider__nav--prev',
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
    })

    const productSlider = new Swiper('.product-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 13,
        navigation: {
            nextEl: '.angle-button--next',
            prevEl: '.angle-button--prev',
        },
    })

    const contentGallerySlider = new Swiper('.content-gallery', {
        loop: false,
        slidesPerView: 1,
        spaceBetween: 13,
        navigation: {
            nextEl: '.slider-button--next',
            prevEl: '.slider-button--prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    })

    $(".btn-scroll").on('click', function () {
        let elementClick = $(this).attr("href");
        let destination = $(elementClick).offset().top;
        $('html, body').animate({ scrollTop: destination }, 600);
        return false;
    });

    function citySwitch() {

        $('.city-switch').on('click', function (e){
            e.preventDefault();

            $.fancybox.open({
                src  : '#places',
                type : 'inline',
                opts : {
                    afterShow : function( instance, current ) {
                        console.info( 'done!' );
                    }
                }
            });

        });

        $('.places__item').on('click', function (e){
            e.preventDefault();
            let $this = $(this);
            let placeName = $this.text();
            let placeText = $this.attr('data-text');

            $('.city-switch__name').text(placeText);
            $.fancybox.close();
        });


    }
    citySwitch();


    function contactSwitch() {

        $('.contacts-switch').on('click', function (e){
            e.preventDefault();

            $.fancybox.open({
                src  : '#places',
                type : 'inline',
                opts : {
                    afterShow : function( instance, current ) {
                        console.info( 'done!' );
                    }
                }
            });

        });

        $('.places__item').on('click', function (e){
            e.preventDefault();
            let $this = $(this);
            let placeName = $this.text();
            let placeText = $this.attr('data-text');

            $('.city-switch__name').text(placeText);
            $('.contacts-switch').text(placeName);
            $.fancybox.close();
        });


    }
    contactSwitch();



});


